Before creating bookmarklet code, run the JS code through JS compressor

https://jscompress.com/ (best)

or

https://www.piliapp.com/minify/yui-compressor/

then Bookmarklet creator, such as

https://chriszarate.github.io/bookmarkleter/ (best)

or

https://caiorss.github.io/bookmarklet-maker/

to ensure the smallest possible bookmarklet code.

If it still turns out to be too big, then try to manually add the changes in the bookmarklet.url (in a compressed way :) yes.. it's fun, but not impossible, just find nearby code to what you're adding).

The size of the bookmarklet is too big for IE11, so best way to use it there is to paste the full code on console, instead of link (bookmarklet).

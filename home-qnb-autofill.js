var $q = $q || function $q(sel) {
  return document.querySelector(sel);
},
a = angular.element,
ai = a(document.body).injector(),
asi = ai.get('sessionStorageFactory'),
ri = function (min, max) {//Get random int
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random()*(max-min+1)+min);
},
rn = function() { //get random name
    return Math.random().toString(36).replace(/[^a-z]+/g, '');
},
qo = asi.getItem('homeQuote') || {}, //My quote object
ud = asi.getItem('uiData') || {}, //uiData object
wt = ['B', 'R', 'F', 'S', 'C'], //allowedWallTypes
gt = [ //gender titles
  {gender: "M", title: "Mr"},
  {gender: "F", title: "Mrs"},
  {gender: "F", title: "Miss"},
  {gender: "F", title: "Ms"}
],
ast = angular.element(document.body).injector().get('multiStepNavigation').getActiveStepNumber() === 14 ? 0 : angular.element(document.body).injector().get('multiStepNavigation').getActiveStepNumber(), //important info step is 14, so display as 0 - handled as 14 down the code
s7nbmsgshown = false,
spn = function(show) {
    if (show) {
        window.setTimeout(function(){$q('[bp-loading-spinner]').classList.remove('ng-hide');},500);
    } else {
        window.setTimeout(function(){$q('[bp-loading-spinner]').classList.add('ng-hide');},500);
    }
},
isIE11 = !!window.MSInputMethodContext && !!document.documentMode,
sc = prompt('Which step'+(isIE11?'':' do you want to Auto Fill to\n0.Important Info\n1: Your Home\n2: Your Details\n3: Your Needs\n4: Your Quote\n5: Review and Confirm\n6: (QA) Payment DD\n7: (QA) Confirmation'),ast), //steps choice
s0c = function(){ //Step 0 code (important-info has a code of 14 in the quote controller steps)
    if (!s7noback()) {
        return false;
    }

    if (ast === 14 && parseInt(sc) === 14) {
        //nothing to do
        alert('You\'re already at first step');
        return false;
    } else {
        qo = ai.get('HomeQuoteModel').factory(asi.getItem('homeQuote') || {});
            
        if (qo.standard && qo.standard.policyHolder) {
            qo.standard.policyHolder.assumptions = true;
        } else {
            qo.standard = {policyHolder: {assumptions: true}};
        }
        
        ai.get('HomeQuoteModel').factory(qo);
        asi.setItem('homeQuote', qo);
        asi.setItem('importantInfoSeen', true);

        if (parseInt(sc) >=1 && parseInt(sc) !== 14) {
            s1c();
        } else {
            ai.get('lazyModuleLoader').goToState('quote.information', 'app.quote.information', {}, true).then(function(){
                window.setTimeout(function(){s1c()},500);
            });
        }

        return true;
    }
},
s1c = function(){ //Step 1 code
    if (!s7noback()) {
        return false;
    }
    if (ast > 1) {
        //skip to step 2
        gbck() && s2c();
    } else {
        spn(true);
        ai.get('postcodeLookup').getAddress('BR1 1DP').success(function(data){
            spn(false);
            //Your Home
            var addrSum, accType;
            qo.standard.address = data[ri(0, data.length-1)];

            qo.standard.address.accommodationType =  ri(1,3).toString();
            qo.standard.address.accommodationTypeParent = 'House';
            qo.standard.propertyDetails.listedBuildingFlag = false;
            qo.standard.propertyDetails.approxYearBuilt = ri(1950,new Date().getUTCFullYear());
            qo.standard.propertyDetails.wallsTypes = wt[ri(0, wt.length-1)];
            qo.standard.propertyDetails.flatRoof =  ri(1,4).toString();
            qo.standard.propertyDetails.noOfBeds =  ri(1,6).toString();
            qo.standard.propertyDetails.noOfBathrooms =  ri(1,4).toString();
            qo.standard.propertyDetails.flooded = false;
            qo.standard.propertyDetails.propertyStatus = ri(1,2).toString();
            qo.standard.ratingDetails.accidentalDamageBuilding = true;
            qo.standard.ratingDetails.accidentalDamageContents = true;
            qo.standard.ratingDetails.buildingFlag = true;
            qo.standard.ratingDetails.contentsFlag = true;
            qo.standard.propertyDetails.noOfAdults =  ri(1,5).toString();
            qo.standard.propertyDetails.noOfChildren =  ri(0,5).toString();
            qo.standard.claims.claimsFreeB = 0;
            qo.standard.claims.claimsFreeC = 0;
            qo.standard.claims.numberOfClaims = 0;
            qo.standard.claims.claimsList = [];
            qo.standard.cover.coverStartDate =  asi.getItem('appConfig').maxCoverStartDate;

            ai.get('HomeQuoteModel').factory(qo);
            asi.setItem('homeQuote', qo);

            if (parseInt(sc) >= 2) {
                s2c();
            } else {
                ai.get('lazyModuleLoader').goToState('quote.home', 'app.quote.home', {}, true);
            }
        });
    }
},
s2c = function(){ //Step 2 code
    if (!s7noback()) {
        return false;
    }
    if (ast > 2) {
        //skip to step 3
        gbck() && s3c();
    } else {
        ai.get('lazyModuleLoader').goToState('quote.details', 'app.quote.details', {}, true);

        //Your Details
        qo.standard.policyHolder = gt[ri(0,gt.length - 1)];

        qo.standard.policyHolder.firstName = rn();
        qo.standard.policyHolder.lastName = rn();
        qo.standard.policyHolder.dateOfBirth = [ri(1950,new Date().getUTCFullYear()-20), ri(1,12), ri(1,28)];
        qo.standard.policyHolder.emailAddress = qo.standard.policyHolder.firstName.toLowerCase() + '.' + qo.standard.policyHolder.lastName.toLowerCase() + '@' + rn().toLowerCase() + '.com';
        qo.standard.policyHolder.phoneNumber = '07' + ri(100000000,999999999);

        qo.standard.preferredPaymentType = ri(0,1) ? 'M' : 'A';
        qo.standard.carRenewalPolicyMonth = ri(1,12);

        ai.get('HomeQuoteModel').factory(qo);
        asi.setItem('homeQuote', qo);

        if (parseInt(sc) >= 3) {
            spn(true);
            ai.get('quoteFactory').postQuote({'quotes': {'STANDARD': qo.standard}}).then(function(postQResp){
                spn(false);

                postQResp.data.quotes.STANDARD.ratingDetails.voluntaryExcess = postQResp.data.quotes.STANDARD.ratingDetails.voluntaryExcess.toString();
                postQResp.data.quotes.ELITE.ratingDetails.voluntaryExcess = postQResp.data.quotes.ELITE.ratingDetails.voluntaryExcess.toString();
                postQResp.data.quotes.STANDARD.propertyDetails.flooded = false;
                postQResp.data.quotes.ELITE.propertyDetails.flooded = false;
                postQResp.data.quotes.STANDARD.policyHolder.assumptions = true;
                postQResp.data.quotes.ELITE.policyHolder.assumptions = true;
                qo.standard = postQResp.data.quotes.STANDARD;
                qo.standard.isPrepop = false;
                qo.elite = postQResp.data.quotes.ELITE;
                qo.elite.isPrepop = false;
                qo.promotionCodeDetails = postQResp.data.promotionCodeDetails || undefined;
                asi.setItem('homeQuote', qo.toJSON());
                asi.setItem('importantInfoSeen', true);
                ai.get('HomeQuoteModel').factory(qo);
                asi.setItem('homeQuote', qo);
                ud.standard = ud.standard || {};
                ud.standard.ratingDetails = ud.standard.ratingDetails || {};
                asi.setItem('uiData', ud);

                s3c();
            });
        }
    }
},
s3c = function(){ //Step 3 code
    if (!s7noback()) {
        return false;
    }
    if (ast > 3) {
        //skip to step 4
        gbck() && s4c();
    } else {
        spn(true);
        ai.get('lazyModuleLoader').goToState('quote.needs', 'app.quote.needs', {}, true).then(function(){
            spn(false);

            if (parseInt(sc) >= 4) {
                ud.needs = ud.needs || {};
                ud.needs.contents = false;
                ud.needs.buildings = false;
                ud.needs.highValue = true;
                ud.needs.bicycles = true;

                asi.setItem('uiData', ud);
                
                s4c();
            }
        });
        
    }
},
s4c = function(){ //Step 4 code
    if (!s7noback()) {
        return false;
    }
    if (ast >= 4) { // the added = solves a problem with requoting when not needed, getting system errors
        //skip to step 5
        if (gbck()) {
            !(parseInt(sc) === 4 && ast === 4) && s5c(); //the extra checks are needed so we don't go to step 5, if the target is 4, coming from the added (and needed >= in `ast >= 4`)
        }
    } else {
        spn(true);
        ai.get('quoteFactory').postQuote({'quotes': {'STANDARD': qo.standard}}).then(function(postQResp){
            spn(false);

            postQResp.data.quotes.STANDARD.ratingDetails.voluntaryExcess = postQResp.data.quotes.STANDARD.ratingDetails.voluntaryExcess.toString();
            postQResp.data.quotes.ELITE.ratingDetails.voluntaryExcess = postQResp.data.quotes.ELITE.ratingDetails.voluntaryExcess.toString();
            postQResp.data.quotes.STANDARD.propertyDetails.flooded = false;
            postQResp.data.quotes.ELITE.propertyDetails.flooded = false;
            postQResp.data.quotes.STANDARD.policyHolder.assumptions = true;
            postQResp.data.quotes.ELITE.policyHolder.assumptions = true;
            qo.standard = postQResp.data.quotes.STANDARD;
            qo.standard.isPrepop = false;
            qo.elite = postQResp.data.quotes.ELITE;
            qo.elite.isPrepop = false;
            qo.promotionCodeDetails = postQResp.data.promotionCodeDetails || undefined;
            asi.setItem('homeQuote', qo.toJSON());
            asi.setItem('importantInfoSeen', true);
            ai.get('HomeQuoteModel').factory(qo);
            asi.setItem('homeQuote', qo);
            ud.standard.ratingDetails = ud.standard.ratingDetails || {};
            asi.setItem('uiData', ud);

            ai.get('lazyModuleLoader').goToState('quote.quote', 'app.quote.quote', {}, true).then(function(){
                window.setTimeout(function(){
                    if (parseInt(sc) >= 5 && $q('button.selectElite')) {
                        s5c();
                    }
                },500);
            });
        });
    }
},
s5c = function(){ //Step 5 code
    if (!s7noback()) {
        return false;
    }
    if (ast > 5) {
        //skip to step 6
        gbck() && s6c();
    } else {
        $q('button.selectElite') && $q('button.selectElite').click();
        //$q('#continueButton').click();
        ai.get('lazyModuleLoader').goToState('quote.summary', 'app.quote.summary', {}, true).then(function(){
            if (parseInt(sc) >= 6) {
                s6c();
            }
        });
    }
},
s6c = function(){ //Step 6 code
    if (!s7noback()) {
        return false;
    }
    if (ast >= 6) { // the added = solves a problem with getting again payment options when not needed
        //skip to step 7
        if (gbck()) {
            !(parseInt(sc) === 6 && ast === 6) && s7c(); //the extra checks are needed so we don't go to step 7, if the target is 6, coming from the added (and needed >= in `ast >= 6`)
        }
    } else {
        window.setTimeout(function(){
            ai.get('lazyModuleLoader').goToState('quote.payment', 'app.quote.payment', {}, true).then(function(){
                if (parseInt(sc) >= 7) {
                    s7c();
                }
            });
        },1500);
    }
},
s7c = function(){ //Step 7 code
    s7noback() && window.setTimeout(function(){
        spn(true);
        ai.get('paymentFactory').validateDirectDebit({
            "instalmentStartDate": '1',
            "isAnnual":false,
            "policyHolder":true,
            "accountName":(qo.standard.policyHolder.title + ' ' + qo.standard.policyHolder.firstName + ' ' + qo.standard.policyHolder.lastName).toUpperCase(),
            "bankName": rn(),
            "maskedAccountNumber":"**** 1110",
            "accountNumber":"11111110",
            "sortCode": "110101"
        }).then(function(data){
            ai.get('paymentFactory').buyDirectDebitPolicy('se').then(function(dataBuy){
                    ai.get('lazyModuleLoader').goToState('quote.confirmation', 'app.quote.confirmation', {}, true);
                    spn(false);
            });
        });
    },1500);
},
lc = location.href,
gbck = function() { //Go to a step
    if (sc < ast) {
        var tS;

        switch (sc) {
            case '6':
                tS = 'payment';
                break;
            case '5':
                tS = 'summary';
                break;
            case '4':
                tS = 'quote';
                break;
            case '3':
                tS = 'needs';
                break;
            case '2':
                tS = 'details';
                break;
            case '1':
                tS = 'home';
                break;
            case '0':
                tS = 'information';
                break;
        }
         ai.get('lazyModuleLoader').goToState('quote.' + tS, 'app.quote.' + tS, {}, true); //Go to target state
         return false;
     } else {
         return true; //so that we can go forward
     }
},
s7noback = function() {
    if (ast === 7 && parseInt(sc) <= 7) {
        //nothing to do
        !s7nbmsgshown && alert('You\'ve completed the journey.' + (parseInt(sc) < 7 ? ' No going back': ''));
        s7nbmsgshown = true;
        return false;
    } else {
        return true;
    }

};

if (sc === 0){ //Important info is actually step 14
    sc = 14;
}

if (parseInt(sc) >= 0) {
    s0c();
    angular.element(document.body).injector().get('ngDialog').closeAll();
}
